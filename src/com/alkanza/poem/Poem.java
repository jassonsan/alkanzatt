package com.alkanza.poem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.Map;

public class Poem {

	private Map<String, Integer> rulesMap;
	private List<Rule> rules;
	private int poemRuleLocation;
	private StringBuilder poem;
	private int globalWords = 0;
	private int maxGlobalWords;
	private final int maxLocalWords = 100;

	public Poem() {
		poem = new StringBuilder();
	}

	public void writePoem() {
		if (poemRuleLocation == -1) {
			poem.append("The grammatical rules must contain a <POEM> rule.");
		} else {
			List<String> data = rules.get(poemRuleLocation).getRulesDefitions();
			// Se necesita una funcion que recorra este arreglo y lo procese,
			poem.append(proccessPoemData(data));
		}
		printPoem();
	}

	private String proccessPoemData(List<String> data) {
		// iteramos
		String ret = "";
		int localwords = 0;
		try {
			List<String> localdata;
			this.globalWords++;
			if (globalWords > maxGlobalWords) {
				printPoemError("Your poem contains more that " + maxGlobalWords
						+ " words, perhaps there is a circular dependency on your rules \n i.e. POEM: <LINE> \n LINE: <POEM>");
			} else {
				for (int i = 0; i < data.size(); i++) {
					// debemos verificar si es data, keyword o regla
					if (getRuleLocation(data.get(i)) != -1) {
						// es regla
						// traemos la regla
						localdata = rules.get(getRuleLocation(data.get(i))).getRulesDefitions();
						// llamado recursivo
						ret = ret + proccessPoemData(localdata);
					} else if (data.get(i).equals("$LINEBREAK")) {
						// es linebreak keyword
						ret = ret + "\n";
					} else if (data.get(i).equals("$END")) {
						// es end keyword, no entiendo bien la utilidad de esta keyword
						return ret;
					} else {
						// es palabra
						localwords++;
						ret = ret + data.get(i) + " ";
						if (localwords > maxLocalWords) {
							printPoemError("Your line contains more that " + maxLocalWords
									+ " words, perhaps there is a circular dependency on your rules \n i.e. POEM: <LINE> \n LINE: <POEM>");
						}
					}
				}
			}
		} catch (Exception e) {
			printPoemError("Poem could not be generate.");
		}

		return ret;
	}

	public void setRulesMap(Map<String, Integer> rulesMap) {
		this.rulesMap = rulesMap;
		this.poemRuleLocation = getRuleLocation("<POEM>");
	}

	public void setRules(List<Rule> rules) {
		this.maxGlobalWords = maxLocalWords * rules.size();
		this.rules = rules;
	}

	private int getRuleLocation(String rule) {
		return this.rulesMap.get(rule) == null ? -1 : this.rulesMap.get(rule);
	}

	private void printPoem() {
		File file = new File("poem.txt");
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
			writer.write(poem.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void printPoemError(String msg) {
		poem = new StringBuilder();
		poem.append(msg);
		printPoem();
	}

}
