package com.alkanza.poem;

import java.util.ArrayList;
import java.util.List;

public class Rule {

	private String name;
	private List<RuleDefinition> definitions;

	public Rule(String name, String definition) {
		this.name = name;
		this.definitions = new ArrayList<>();
		//el espacio significa "y" el pipe significa "o"
		// se deben verificar los "y"
		String[] parts = definition.trim().split(" ");
		String parts2[];
		for (int i = 0; i < parts.length; i++) {
			this.definitions.add(new RuleDefinition(parts[i]));
		}
	}
	
	public List<String>  getRulesDefitions() {
		List<String> rulesDefinitions = new ArrayList<>();
		for(RuleDefinition definition: this.definitions) {
			rulesDefinitions.add(definition.getRuleValue());
		}
		
		return rulesDefinitions;
	}
}
