package com.alkanza;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alkanza.poem.Poem;
import com.alkanza.poem.Rule;

public class PoemGenerator {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.setIn(new FileInputStream("input.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		String line = bf.readLine();
		Poem poem = new Poem();
		Map<String, Integer> rulesMap = new HashMap<>();
		List<Rule> rules = new ArrayList<>();
		Rule rule;
		int ruleCount = 0;
		String[] parts;
		boolean validInput = true;
		while (line != null) {
			parts = line.trim().split(":");
			line = bf.readLine();
			// parts 0 debe tener el nombre de la regla
			// parts 1 debe tener la definicion de la regla
			validInput = parts.length == 2 ? true : false;
			if (validInput) {
				rule = new Rule(parts[0], parts[1]);
				rulesMap.put("<" + parts[0] + ">", ruleCount);
				ruleCount++;
				rules.add(rule);
			} else {
				//el input esta en lineas diferentes o tiene un numero de : diferente de 1
				line = null;
				poem.printPoemError("There is an input error, the name of the rule and it`s definition should be in the same line.\n i.e. LINE: <NOUN>|<PREPOSITION>|<PRONOUN> $LINEBREAK");
			}
		}
		if (validInput) {
			poem.setRulesMap(rulesMap);
			poem.setRules(rules);
			poem.writePoem();
		}
	}

}
