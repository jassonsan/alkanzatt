package com.alkanza.poem;

import java.util.ArrayList;
import java.util.List;

import com.alkanza.utilities.Util;

public class RuleDefinition {
	private List<String> definition;

	public RuleDefinition(String def) {
		this.definition = new ArrayList<>();
		String[] parts = def.trim().split("\\|");
		for (int i = 0; i < parts.length; i++) {
			this.definition.add(parts[i]);
		}
	}

	public List<String> getDefinition() {
		return definition;
	}

	public String getRuleValue() {
		return this.definition.size() > 1 ? Util.getRandomWord(this.definition) : this.definition.get(0);
	}

}
